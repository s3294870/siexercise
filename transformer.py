import sys
import pandas as pd
from rdflib import Graph, Literal, RDF, Namespace, URIRef
from rdflib.namespace import XSD
import os

# namespaces
SAREF = Namespace("https://saref.etsi.org/core/")
EX = Namespace("http://example.org/")

def load_csv(file_path):
    return pd.read_csv(file_path)

def create_rdf_graph(data):
    g = Graph()
    g.bind("saref", SAREF)
    g.bind("ex", EX)

    timestamp_column = 'utc_timestamp'  

    for index, row in data.iterrows():
        timestamp = row[timestamp_column]
        
        for col in data.columns:
            if col != timestamp_column:
                device_name = col
                measurement_value = row[col]
                device = URIRef(EX[device_name])
                g.add((device, RDF.type, SAREF.Device))
                
                measurement = URIRef(EX[f"measurement_{index}_{device_name}"])
                g.add((measurement, RDF.type, SAREF.Measurement))
                g.add((measurement, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))
                g.add((measurement, SAREF.hasValue, Literal(measurement_value, datatype=XSD.float)))
                g.add((measurement, SAREF.isMeasuredBy, device))
    
    return g

def save_graph(graph, file_name):
    graph.serialize(destination=file_name, format='turtle')

def main(file_path):
    data = load_csv(file_path)
    print("Columns in CSV file:", data.columns)
    rdf_graph = create_rdf_graph(data)
    save_graph(rdf_graph, "graph.ttl")
    print("RDF graph saved to graph.ttl")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python transformer.py path/to/dataset.csv")
    else:
        csv_path = sys.argv[1]
        main(csv_path)
